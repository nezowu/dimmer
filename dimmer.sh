#!/bin/bash
  
stdbuf -o0 fswebcam --greyscale --no-banner -qr 1x1 --scale 1x1 --jpeg -1 -l 1 - |
  stdbuf -o0 ffmpeg -v quiet -f mjpeg -i - -f rawvideo -pix_fmt gray - |
    stdbuf -o0 hexdump -ve '1/1 "%u" "\n"' |
      awk '
BEGIN	{"light -G" | getline; f = int($1)}
	{$1=int($1*0.5)*3; k=1}
$1>100  {$1 = 100}
#!f      {f = $1 - 1}
f==$1	{next}
int(($1-f)/10) {k=2}
	{d = f>$1?-1:1
	while(f != $1) {
	  f+=d
	  if(!(f%k)) {printf "%u ", f
	    system("light -S "f"; sleep 0.05")
	  }
	}
	f=$1
}'
echo
